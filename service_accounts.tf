resource "kubernetes_service_account_v1" "this" {
  for_each = {
    for k, v in var.users_list:
    k => v.name
  }

  metadata {
    name = each.value
  }
}