output "secrets_access_tokens" {
  value     = "${data.kubernetes_secret_v1.content[*].data.token}"
}
