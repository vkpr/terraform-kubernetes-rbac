# Kubernetes RBAC - Users | Terrafom Module

Módulo do Terraform para criação de usuários para usso no cluster Kubernetes.

## :rocket: Como usar

Para utilizar o módulo é necessário gerar um [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) do GitLab e configurá-lo na máquina local ou pipeline para que o Terraform utilize. A seguinte configuração precisa ser criada em `~/.terraformrc`:

```hcl
credentials "gitlab.com" {
  token = "<GITLAB_ACCESS_TOKEN>"
}
```

### Exemplo

O exemplo abaixo demonstra o mínimo de configuração necessária para utilização do módulo.

```hcl
module "kubernetes" {
  source  = "gitlab.com/vertigobr/terraform-kubernetes-rbac/kubernetes"
  version = "1.0.0"

  cluster_endpoint       = "<CLUSTER_ENDPOINT>"
  cluster_ca_certificate = "<CLUSTER CERTIFICATE AUTHORITY>"
  cluster_access_token   = "<KUBECONFIG_TOKEN>"
}
```

### Providers
 
| Nome | Versão |
|------|--------|
| [kubernetes](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.0) | ~> 2.12.0 |

### Modules

Sem utilização de módulos.

### Resources

| Nome | Tipo |
|------|------|
| [kubernetes_secret_v1.content](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data/secret_v1) | datasource |
| [kubernetes_service_account_v1.this](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account_v1) | resource |
| [kubernetes_cluster_role_binding_v1.*](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cluster_role_binding_v1) | resource |

### Inputs

| Nome | Descrição | Tipo | Valor padrão | Obrigatório |
|------|-----------|------|--------------|-------------|
| cluster_endpoint | Hostname da API do Kubernetes. | `string` | n/a | **Sim** |
| cluster_ca_certificate | Certificados codificados por PEM para autenticação TLS com a API do Kubernetes. | `string` | n/a | **Sim** |
| cluster_access_token | Cluster Acess token. | `string` | n/a | **Sim** |
| users_list | Lista de usuários a serem criados no Cluster. | `list(map(string))` | `"[{name = "root-user",role = "root"}]"` | Não |

### Tipos de usuários

- root
- admin
- editor
- viewer

### Outputs

| Nome | Descrição |
|------|-----------|
| secrets_access_tokens | Tokens dos `service account` para autenticação na API do Kubernetes. |

## :memo: Licença

[MIT](LICENSE)
