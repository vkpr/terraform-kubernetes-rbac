### Kubernetes Connection
variable "cluster_endpoint" {
  description = "Endereço do endpoint do CP do cluster."
  type        = string
}

variable "cluster_ca_certificate" {
  description = "Certificado de acesso ao CP."
  type        = string
}

variable "cluster_access_token" {
  description = "Token para acesso ao cluster."
  type        = string
}

### Kubernetes root role

variable "users_list" {
  type = list(map(string))
  default = [{
    name = "root-user",
    role = "root"
  }]
}