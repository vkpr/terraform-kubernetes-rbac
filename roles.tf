resource "kubernetes_cluster_role_binding_v1" "root" {
  for_each = { 
    for key, value in var.users_list:
    key => value
    if value.role == "root"
  }

  metadata {
    name = "${each.value.name}-role"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = each.value.name
    api_group = ""
  }
}

resource "kubernetes_cluster_role_binding_v1" "admin" {
  for_each = { 
    for key, value in var.users_list:
    key => value
    if value.role == "admin"
  }

  metadata {
    name = "${each.value.name}-role"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = each.value.name
    api_group = ""
  }
}

resource "kubernetes_cluster_role_binding_v1" "editor" {
  for_each = { 
    for key, value in var.users_list:
    key => value
    if value.role == "editor"
  }

  metadata {
    name = "${each.value.name}-role"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "editor"
  }
  subject {
    kind      = "ServiceAccount"
    name      = each.value.name
    api_group = ""
  }
}

resource "kubernetes_cluster_role_binding_v1" "viewer" {
  for_each = { 
    for key, value in var.users_list:
    key => value
    if value.role == "viewer"
  }

  metadata {
    name = "${each.value.name}-role"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "viewer"
  }
  subject {
    kind      = "ServiceAccount"
    name      = each.value.name
    api_group = ""
  }
}