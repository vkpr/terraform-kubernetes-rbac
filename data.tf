data "kubernetes_secret_v1" "content" {
  count = length(kubernetes_service_account_v1.this)

  metadata {
    name = "${kubernetes_service_account_v1.this[count.index].default_secret_name}"
  }
}
